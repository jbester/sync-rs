use std::sync::{Mutex, Condvar};

/// A StartGroup provides a mechanism for a collection of threads to wait for a release event.
/// When released, all blocked routines simultaneously.
pub struct StartGroup {
    state: Mutex<i32>,
    cond: Condvar,
}

impl StartGroup {
    pub fn new() -> StartGroup {
        StartGroup {
            state: Mutex::new(0),
            cond: Condvar::new()
        }
    }

    ///  Get the current count of waiting threads
    pub fn waiting(&self) -> i32 {
        return *self.state.lock().unwrap()
    }

    /// Release all threads
    pub fn release(&self) {
        let mut waiting = self.state.lock().unwrap();
        *waiting = 0;
        self.cond.notify_all()
    }

    /// Wait for the release event
    pub fn wait(&self) {
        let mut l = self.state.lock().unwrap();
        *l += 1;
        let result = self.cond.wait(l);
        match result {
            Ok(_) => (),
            Err(_) => panic!()
        };
    }

}
//
//#[test]
//fn test_wait() {
//    let waiter = Arc::new(StartGroup::new());
//    let waker = waiter.clone();
//    let (tx, rx) = sync_channel::<bool>(1);
//    let child = thread::spawn( move|| {
//        waiter.wait();
//        tx.send(true).unwrap();
//    });
//    // allow thread to start up
//    thread::sleep(Duration::from_millis(10));
//
//    // verify nothing is pending
//    assert!(match rx.try_recv() {
//        Ok(_) => false,
//        Err(_) => true
//    });
//
//    // release
//    waker.release();
//
//    // wait for completion
//    child.join().unwrap();
//
//    // verify it completed normally
//    assert!(rx.recv().unwrap());
//}
//
//#[test]
//fn test_wait_count() {
//    let waiter1 = Arc::new(StartGroup::new());
//    let waiter2 = waiter1.clone();
//    let waker = waiter1.clone();
//    let child1 = thread::spawn( move|| {
//        waiter1.wait();
//    });
//    let child2 = thread::spawn( move|| {
//        waiter2.wait();
//    });
//
//    // allow thread to start up
//    thread::sleep(Duration::from_millis(10));
//
//    assert_eq!(2, waker.waiting());
//
//    // release
//    waker.release();
//
//    // wait for completion
//    child1.join().unwrap();
//    child2.join().unwrap();
//
//    // verify it completed normally
//    assert_eq!(0, waker.waiting());
//}
