/// The events module provides a single synchronization primitive the Event. An event is used to
/// notify the occurrence of a condition to threads.
///
/// Multiple threads can wait on a condition. All routines unblock once the condition occurs.
/// A routine that waits on a condition that has already occurred will not block.
///
/// The event primitive is similar to the event in the pSOS or ARINC 653 API sets.
pub mod events;

#[cfg(test)]
mod events_tests;

/// The start_group package provides a mechanism for a collection of threads to wait for a
/// release event. When released, all blocked routines are released simultaneously.
///
/// A archetypal use is when multiple routines need to know when a resource is available but
/// do not need exclusive access to the resource.
pub mod start_group;

#[cfg(test)]
mod start_group_tests;

