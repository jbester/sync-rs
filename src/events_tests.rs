use std::time::{Duration};
use std::time::{Instant};
use events::{Event};

#[test]
fn test_event_basic_operations() {
    // new event
    let evt = Event::new();
    // verify cleared
    assert!(evt.is_clear());

    // set and verify set
    evt.set();
    assert!(evt.is_set());

    // clear again and verify cleared
    evt.clear();
    assert!(evt.is_clear());
}

#[test]
fn test_wait_timeout() {
    // new event cleared
    let evt = Event::new();

    // wait for it
    let r = evt.wait_timeout(Duration::from_millis(10));

    // verify timeout
    assert!(!r);
}

#[test]
fn test_wait_no_timeout() {
    const TIMEOUT_MS: u64 = 10;
    // new set event
    let evt = Event::new();
    evt.set();

    // try to wait for it
    let start = Instant::now();
    let r = evt.wait_timeout(Duration::from_millis(TIMEOUT_MS));
    let end = Instant::now();

    // verify wait took less than timeout
    assert!(r);
    assert!((end - start) < Duration::from_millis(TIMEOUT_MS));
}