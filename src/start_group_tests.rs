use std::sync::{Arc};
use std::sync::mpsc::{sync_channel};
use std::time::{Duration};
use std::thread;
use start_group::{StartGroup};

#[test]
fn test_wait() {
    let waiter = Arc::new(StartGroup::new());
    let waker = waiter.clone();
    let (tx, rx) = sync_channel::<bool>(1);
    let child = thread::spawn( move|| {
        waiter.wait();
        tx.send(true).unwrap();
    });
    // allow thread to start up
    thread::sleep(Duration::from_millis(10));

    // verify nothing is pending
    assert!(match rx.try_recv() {
        Ok(_) => false,
        Err(_) => true
    });

    // release
    waker.release();

    // wait for completion
    child.join().unwrap();

    // verify it completed normally
    assert!(rx.recv().unwrap());
}

#[test]
fn test_wait_count() {
    let waiter1 = Arc::new(StartGroup::new());
    let waiter2 = waiter1.clone();
    let waker = waiter1.clone();
    let child1 = thread::spawn( move|| {
        waiter1.wait();
    });
    let child2 = thread::spawn( move|| {
        waiter2.wait();
    });

    // allow thread to start up
    thread::sleep(Duration::from_millis(10));

    assert_eq!(2, waker.waiting());

    // release
    waker.release();

    // wait for completion
    child1.join().unwrap();
    child2.join().unwrap();

    // verify it completed normally
    assert_eq!(0, waker.waiting());
}
