use std::sync::{Mutex, Condvar};
use std::time::{Duration};

/// Multiple threads can wait on a condition.   _All_ threads unblock once the event is set
/// to the set state.  A thread that waits on an event that is already set will not block.
pub struct Event {
    state: Mutex<bool>,
    cond: Condvar,
}

impl Event {
    pub fn new() -> Event {
        Event {
            state: Mutex::new(false),
            cond: Condvar::new()
        }
    }

    /// Test if the event is set
    pub fn is_set(&self) -> bool {
        return *self.state.lock().unwrap()
    }

    /// Test if the event is cleared
    pub fn is_clear(&self) -> bool {
        return !self.is_set()
    }

    ///  Set the event and unblock any waiting threads.
    ///  Returns true on change.
    pub fn set(&self) -> bool {
        let mut flag = self.state.lock().unwrap();
        let changed = !*flag;
        if changed {
            *flag = true;
            self.cond.notify_all();
        }
        changed
    }

    ///  Clear the event and start blocking  any waiting threads.
    ///  Returns true on change.
    pub fn clear(&self) -> bool {
        let mut flag = self.state.lock().unwrap();
        let changed = *flag;
        *flag = false;
        changed
    }

    /// Wait for the event to be set
    pub fn wait(&self) {
        let l = self.state.lock().unwrap();
        if *l {
            return
        } else {
            let result = self.cond.wait(l);
            match result {
                Ok(_) => (),
                Err(_) => panic!()
            }
        }
    }

    /// Wait for the event to be set.
    /// Returns true if event set, otherwise false.
    pub fn wait_timeout(&self, timeout: Duration) -> bool {
        let l = self.state.lock().unwrap();
        if *l {
            true
        } else {
            let result = self.cond.wait_timeout(l, timeout);
            match result {
                Ok((_, r)) => !r.timed_out(),
                Err(_) => false
            }
        }
    }
}
