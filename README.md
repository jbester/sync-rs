Sync - Additional synchronization primtives
===========================================

Rust set of packages that provide synchonization primitives beyond those provided by the sync package.

Primitives include:

-	[Events](#events)
-	[StartGroups](#start-group)


events package
------------------------------------------------------------------------------------------

The `events` package provides a single synchronization primitive the Event. An event is used to notify the occurrence of a condition to threads.

Multiple threads can wait on a condition. *All* threads unblock once the condition occurs. A threade that waits on a condition that has already occurred will not block.

The event primitive is similar to the event in the pSOS or ARINC 653 API sets.

start_group package
--------------------------------------------------------------------------------------------------

The `start_group` package provides a mechanism for a collection of threads to wait for a release event. When released, all blocked threads simultaneously.

A typical use is when multiple threads need to know when a resource is available but do not need exclusive access to the resource.

Contributing
============

Please feel free to submit issues, fork the repository and send pull requests!

---

Licence
=======

MIT License - see LICENSE file
